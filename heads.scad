include <settings.scad>


heads();


module heads() {
    cylinder(h=base_thickness/2, r=base_radius);
    translate([0, 0, base_thickness / 2]) {
        powder_line();
        snuff_tube();
    }
}


module powder_line() {
    random_x = rands(line_corn_r, line_length - line_corn_r, line_amount);
    random_y = rands(line_corn_r, line_height - line_corn_r, line_amount);

    linear_extrude(2, center=true)
    translate([-line_length / 2, -line_offset, 0]) {
        for (i=[0:line_amount]) {
            translate([random_x[i], random_y[i]])
                circle(line_corn_r);
        }
    }
}


module snuff_tube() {
    translate(tube_position)
    rotate([0, 90, 70])
    difference() {
        cylinder(h=tube_length, r=tube_r);
        cylinder(h=tube_length, r=tube_r-tube_wall_thickness);
        translate([0, -tube_length, 0])
            cube(2 * tube_length); // cuts it in half
    }
}
