include <settings.scad>


tails();


module tails(engraved=false) {
    if (engraved) {
        difference() {
            cylinder(h=base_thickness/2, r=base_radius);
            translate([0, 0, base_thickness / 2 - 0.5])
                x_months_letters();
        }
    } else {
        cylinder(h=base_thickness / 2, r=base_radius);
        translate([0, 0, base_thickness / 2])
            x_months_letters();
    }
}


module x_months_letters() {
    linear_extrude(1) {
        rotate([0, 0, months_str_angle])
            curved_text(months_str);
        rotate([0, 0, clean_str_angle])
            curved_text("clean");
    }

    linear_extrude(1) {
        text(
            str(months),
            halign="center",
            valign="center",
            size=number_font_size,
            font=number_font
        );
    }
}


// Found somewhere on the internet and adapted:
module curved_text(txt, valign="baseline") {
  a = 180 * curved_font_size * curved_spacing / (PI * curved_r);
  for (i = [0:len(txt)-1])
    rotate([0, 0, -(i + 0.5) * a])
    translate([0, curved_r])
        text(
            txt[i],
            size=curved_font_size,
            halign="center",
            valign=valign,
            $fn=32,
            font=curved_font
        );
}

