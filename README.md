# X Months Chip

This 3D printable chip was created to help staying clean from sniffing powder drugs.

## How to build
- Edit `settings.scad` (the first setting is the most important).
- For each part in `heads.scad`, `tails.scad` and `ring.scad` open the file in [OpenSCAD](https://openscad.org/), render it (press F6) and export the STL with the STL button.
If you have `make` installed a simple `make all` does it all automatically.
- Slice and print the STL files
- Glue them together

## Pictures
![The tails side of a coin with a big 11 in the middle and "Months clean" as curved text around it](pictures/tails.png)
![The heads side of a coin with a snuff tube and a line of powder coming out of it](pictures/heads.png)
