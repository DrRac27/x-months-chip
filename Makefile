OPENSCAD := openscad
BUILD_DIR := ./output
PICS_DIR := ./pictures
ALL_PARTS = heads tails ring

.PHONY: all
all:
	for i in $(ALL_PARTS); do \
		make $$i; \
	done

.PHONY: clean
clean:
	rm -r $(BUILD_DIR)

.PHONY: $(ALL_PARTS)
$(ALL_PARTS):
	$(MAKE) $(BUILD_DIR)/$@.stl

.PHONY: pictures
pictures:
	$(MAKE) $(PICS_DIR)/heads.png
	$(MAKE) $(PICS_DIR)/tails.png

$(BUILD_DIR)/%.stl: %.scad settings.scad
	mkdir -p $(BUILD_DIR)
	$(OPENSCAD) $(*).scad -o $(BUILD_DIR)/$(*).stl

$(PICS_DIR)/heads.png: *.scad
	mkdir -p $(PICS_DIR)
	$(OPENSCAD) chip.scad -o $(PICS_DIR)/heads.png --camera 0,0,0,53,0,28,140 --render I_DONT_KNOW_WHAT_ARG_MEANS

$(PICS_DIR)/tails.png: *.scad
	mkdir -p $(PICS_DIR)
	$(OPENSCAD) chip.scad -o $(PICS_DIR)/tails.png --camera 0,0,0,230,0,330,140 --render I_DONT_KNOW_WHAT_ARG_MEANS
