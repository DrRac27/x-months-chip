// Change this on every print:
months = 11;

// General preferences:
$fn = 500;
base_thickness = 3;
base_radius = 20;
ring_thickness = base_thickness + 1;
ring_radius = base_radius + 2;

// Preferences for the curved text
number_font = "DejaVu Sans Mono:style=Bold";
number_font_size = 13;
months_str = "Months";
// months_str = "Monate";
months_str_angle = 67.5;
clean_str_angle = -120;
curved_font = number_font;
curved_font_size = 6;
curved_r = base_radius - curved_font_size - 2;
curved_spacing = 0.8;

// Preferences for the powder line
line_length = 30;
line_height = 3.5;
line_offset = 10;
line_corn_r = 0.5;
line_amount = 200;

// Preferences for the snuff tube
tube_r = 2.5;
tube_length = 20;
tube_position = [-line_length / 2 + tube_r, -5, 2 - tube_r];
tube_wall_thickness = 0.5;
