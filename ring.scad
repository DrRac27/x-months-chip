include <settings.scad>


ring();


module ring() {
    difference() {
        cylinder(h=ring_thickness, r=ring_radius, center=true);
        cylinder(h=ring_thickness, r=base_radius, center=true);
    }
}
